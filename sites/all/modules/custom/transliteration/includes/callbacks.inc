<?php
define('TRANSLITERATION_MODULE_PATH', drupal_get_path('module', 'transliteration'));
define('TRANSLITERATION_MODULE_PATH_TO_CSS', TRANSLITERATION_MODULE_PATH . '/css');

/**
 * Callback для хук menu..
 */
function transliteration_callback($form, &$form_state) {

  if (isset($form_state['translit_result'])) {
    $markup_value = $form_state['translit_result']['#markup'];
  }
  else {
    $markup_value = '';
  }

  $form['translit_result'] = array(
    '#markup' => $markup_value,
    '#prefix' => '<div id="translit-result">',
    '#suffix' => '</div>',
    '#weight' => 1,
  );

  $form['translit_area'] = array(
    '#title' => 'Автоматическая транслитерация',
    '#type' => 'textarea',
    '#description' => 'Транслитерация по ГОСТ 7.79-2000 Система Б',
    '#weight' => 2,
  );

  $form['translit_deviation'] = array(
    '#type' => 'checkbox',
    '#title' => 'Перевод производиться по частоупотребимым отступлениям от стандарта:',
    '#default_value' => FALSE,
    '#return_value' => TRUE,
    '#weight' => 3,
  );


  $form['translit_deviation_table'] = array(
    '#markup' => '<table>
                  <tr><td class="original">х</td><td>=></td><td class="result">kh</td></tr>
                  <tr><td class="original">ц</td><td>=></td><td class="result">ts</td></tr>
                  <tr><td class="original">ы</td><td>=></td><td class="result">y</td></tr>
                  <tr><td class="original">ъ</td><td>=></td><td class="result">"</td></tr>
                  <tr><td class="original">ь</td><td>=></td><td class="result">&#39</td></tr>
                  <tr><td class="original">э</td><td>=></td><td class="result">eh</td></tr>
                  </table>',
    '#prefix' => '<div id="translit-deviation-table">',
    '#suffix' => '</div>',
    '#weight' => 4,
  );

  $form['translit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Перевод'),
    '#submit' => array('transliteration_data_submit_callback'),
    '#ajax' => array(
      'callback' => 'transliteration_ajax_insert',
      'wrapper' => 'translit-result',
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#weight' => 5,
  );

  $form['#attached']['css'] = array(TRANSLITERATION_MODULE_PATH_TO_CSS . '/form.css');

  return $form;
}

/**
 * Callback для submit..
 */
function transliteration_data_submit_callback($form, &$form_state) {
  $form_state['translit_result']['#markup'] = transliteration($form_state['values']['translit_area'], $form_state['values']['translit_deviation']);
  $form_state['rebuild'] = TRUE;
}

/**
 * Вставка аяксом ..
 */
function transliteration_ajax_insert($form, &$form_state) {
  return $form['translit_result'];
}

/**
 * Функция транслитерации..
 */
function transliteration($text, $translit_deviation) {
  $translit = array(
    "а" => "a",
    "б" => "b",
    "в" => "v",
    "г" => "g",
    "д" => "d",
    "е" => "e",
    "ё" => "yo",
    "ж" => "zh",
    "з" => "z",
    "и" => "i",
    "й" => "j",
    "к" => "k",
    "л" => "l",
    "м" => "m",
    "н" => "n",
    "о" => "o",
    "п" => "p",
    "р" => "r",
    "с" => "s",
    "т" => "t",
    "у" => "u",
    "ф" => "f",
    "х" => "x",
    "ц" => "cz",
    "ч" => "ch",
    "ш" => "sh",
    "щ" => "shh",
    "ъ" => "&#96&#96",
    "ы" => "y'",
    "ь" => "&#96",
    "э" => "e&#96",
    "ю" => "yu",
    "я" => "ya",
    "А" => "A",
    "Б" => "B",
    "В" => "V",
    "Г" => "G",
    "Д" => "D",
    "Е" => "E",
    "Ё" => "Yo",
    "Ж" => "Zh",
    "З" => "Z",
    "И" => "I",
    "Й" => "J",
    "К" => "K",
    "Л" => "L",
    "М" => "M",
    "Н" => "N",
    "О" => "O",
    "П" => "P",
    "Р" => "R",
    "С" => "S",
    "Т" => "T",
    "У" => "U",
    "Ф" => "F",
    "X" => "X",
    "Ц" => "CZ",
    "Ч" => "Ch",
    "Ш" => "Sh",
    "Щ" => "Shh",
    "Ъ" => "&#96&#96",
    "Ы" => "Y'",
    "Ь" => "&#96'",
    "Э" => "E&#96",
    "Ю" => "Yu",
    "Я" => "Ya",
  );

  if ($translit_deviation) {
    $translit["х"] = "kh";
    $translit["Х"] = "Кh";
    $translit["ц"] = "ts";
    $translit["Ц"] = "Ts";
    $translit["ы"] = "y";
    $translit["Ы"] = "Y";
    $translit["ъ"] = '"';
    $translit["Ъ"] = '"';
    $translit["ь"] = "'";
    $translit["Ь"] = "'";
    $translit["э"] = "eh";
    $translit["Э"] = "Eh";
  }
  $translit_string = strtr($text, $translit);
  $replace_array = array('CZI', 'CZE', 'CZY', 'CZJ');
  foreach ($replace_array as $item) {
    $replace_string = str_replace('Z', '', $item);
    $translit_string = str_replace($item, $replace_string, $translit_string);
  }
  return $translit_string;
}


